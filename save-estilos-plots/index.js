  /**
 * Script para salvar gráficos de participantes em uma dinâmica.
 * Input: cpf_consultor, senha_consultor, nome_cliente, nome_projeto, nome_dinâmica
 * Output: pasta output com 2 arquivos para cada participante, cada arquivo nomeado:
 * <nome_participante>_grafico estilo_<unique_id>
 * 
 */

const puppeteer = require('puppeteer')
const bcrypt = require("bcrypt");
const path = require("path")

// CONSTANTES
const IAPP_URL = 'http://3.19.96.101:2000//#/login'

const OUT_PATH = path.join(".", "output")
const TIMEOUT = 1000 * 60

const input = {
  cpf: '',
  senha: '',
  cliente: '',
  projeto: '',
  dinamica: ''
}

const selectors = {
  login: {
    cpf: 'input[name="cpf"]',
    senha: 'input[name="senha"]',
    button: 'button'
  },
  sidebar: {
    toggler: '.navbar-toggler-icon',
    meus_processos: 'a[href="#/clientes"]'
  },
  listaClientes: {
    item: '.sc-fznKkj',
    button: idx => '#icon-list-' + idx
  },
  listaProjetos: {
    item: '.sc-fznKkj', // usado para descobrir idx de div com innerText que dê match no nome do projeto
    button: '#icon-acompanhar' // usado para clickar (precisa do idx)
  },
  sidebarProjeto: {
    buttonDinamicas: '#icon-projeto-sincronos'
  },
  listaDinamicas: {
    item: '.sc-fznyAO',
    button: '#icon-acompanhar'
  },
  acompanhamentoDinamica: {
    participanteRodada: '.sc-pZaHX',
    closeModal: ".fade.modal"
  },
  graficoEstilos: {
    eficaciaHeader: '.apexcharts-annotations',
    legenda: '.apexcharts-legend-text'
  }
}

const buildFileName = async (cliente, projeto, dinamica, participantName, participantCpf) => {
  let hashIn = `${cliente}#${projeto}#${dinamica}#${participantCpf}`
  let numero = await bcrypt.hash(hashIn, 10)
  return `${participantName}_grafico estilo_${numero.replace('/', '%').replace('.', '#').replace('\\', '+').slice(7, 13)}.png`
}

const run = async () => {
  try {
    const browser = await puppeteer.launch({
      defaultViewport: null,
      args: [
        '--window-size=1920,1920',
        '--no-sandbox',
        '--disable-setuid-sandbox'
      ]   
    }, )
    const page = await browser.newPage();
    const login = async () => {
      try {
        await page.goto(IAPP_URL, { timeout: TIMEOUT })
        await page.type(selectors.login.cpf, input.cpf)
        await page.type(selectors.login.senha, input.senha);
        await page.click(selectors.login.button);
        return true;
      } catch (err) {
        console.error("Erro ao fazer login do consultor");
        throw err;
      }
    }

    const goToClients = async () => {
      try {
        try {
          await page.waitForSelector(selectors.sidebar.meus_processos, { timeout: TIMEOUT })
          await page.click(selectors.sidebar.meus_processos);
        } catch (err) { // as vezes meus_processos começa dentro da sidebar então as linhas acima dão erro e caímos no catch
            await page.waitForSelector(selectors.sidebar.toggler, { timeout: TIMEOUT }); // espera o toggler aparecer
            await page.click(selectors.sidebar.toggler); // clicka no toggler
            await page.waitForSelector(selectors.sidebar.meus_processos, { timeout: TIMEOUT});
            await page.click(selectors.sidebar.meus_processos);
        }
      } catch(err) {
        console.error("Erro ao tentar ir para projetos");
        throw err;
      }
    }

    const goToProjects = async () => {
      await page.waitForSelector(selectors.listaClientes.item, { timeout: TIMEOUT })
      let clientIndex = await page.evaluate((clientName, itemSelector) => {
        let divs = Array.from(document.querySelectorAll(itemSelector));
        return divs.filter(d => d.innerText.includes(clientName)).map((_, i) => i).reverse()[0];
      }, input.cliente, selectors.listaClientes.item)
      await page.click(selectors.listaClientes.button(clientIndex));
    }

    async function screenshotDOMElement(selector, padding = 0, first = 0, filename) {
      const rect = await page.evaluate((selector, first) => {
        const elements = document.querySelectorAll(selector);
        let element = null;
        if (!first) element = elements;
        else if (first === 1) element = elements[0];
        else element = elements[elements.length-1]
        const {x, y, width, height} = element.getBoundingClientRect();
        return {left: x, top: y, width, height, id: element.id};
      }, selector, first);
    
      return await page.screenshot({
        path: path.join(OUT_PATH, filename || 'element.png'),
        clip: {
          x: rect.left - padding,
          y: rect.top - padding,
          width: rect.width + padding * 2,
          height: rect.height + padding * 2
        },
      });
    }

    const goToDinamicas = async () => {
      await page.waitForSelector(selectors.listaProjetos.item, { timeout: TIMEOUT });
      let projectIdx = await page.evaluate((itemSelector, projectName) => {
        return Array.from(
          document.querySelectorAll(itemSelector)
        ).findIndex(tag => tag.innerText.includes(projectName))
      }, selectors.listaProjetos.item, input.projeto)
      await page.evaluate((projectIdx, buttonSelector) => {
        document.querySelectorAll(buttonSelector)[projectIdx].click()
      }, projectIdx, selectors.listaProjetos.button)
    }

    const goToProjectAcompanhamento = async () => {
      await page.waitForSelector(selectors.sidebarProjeto.buttonDinamicas, { timeout: TIMEOUT });
      await page.click(selectors.sidebarProjeto.buttonDinamicas);
    }

    const goToDinamicaAcompanhamento = async () => {
      await page.waitForSelector(selectors.listaDinamicas.item, { timeout: TIMEOUT });
      let dinamicaIdx = await page.evaluate((itemSelector, dinamica) => {
        return Array.from(
          document.querySelectorAll(
            itemSelector
          )
        ).findIndex(tag => tag.innerText.includes(dinamica))
      }, selectors.listaDinamicas.item, input.dinamica)
      await page.evaluate((buttonSelector, dinamicaIdx) => {
        document.querySelectorAll(buttonSelector)[dinamicaIdx].click()
      }, selectors.listaDinamicas.button, dinamicaIdx)
    }

    /**
     * @returns {{ cpf: string, name: string, selector: string }[]} dados de participantes
     */
    const getParticipants = async () => {
      // await page.waitForSelector(selectors.acompanhamentoDinamica.participanteRodada)
      await page.waitFor(TIMEOUT);
      return page.evaluate((participantSelector) => {
        return Array.from(
          document.querySelectorAll(participantSelector)
        )
        .map(div => div.innerText)
        .map((participantText, idx) => participantText.split('|').concat(idx))
        .map(([ name, cpf, status, index ]) => ({ name: name.trim(), cpf: cpf.trim(), index }))
      }, selectors.acompanhamentoDinamica.participanteRodada)
    }

    const takeSSOfParticipantEstilos = async (participante) => {
      const indexParticipante = participante.index;
      await page.waitForSelector(selectors.acompanhamentoDinamica.participanteRodada, { timeout: TIMEOUT})
      await page.evaluate((itemSelector, pos) => {
        document.querySelectorAll(itemSelector)[pos].click()
      }, selectors.acompanhamentoDinamica.participanteRodada, indexParticipante)
      const folderNameSuperSet = ".without-margin" // seleciona tags de uma árvore que contém os headers dos folders
      const openFolder = '.open-folder'
      const clickHeaderFolder = async (name, last = false) => {
        
        return page.evaluate((folderNameSuperSet, name, last) => {
          let f = (name, clickLast) => {
            const tags = Array.from(document.querySelectorAll(folderNameSuperSet)).filter(tag => tag.innerText === name);
            const [ first ] = tags;
            const [ last ] = tags.reverse();
            if (clickLast) last.click()
            else first.click()
          }
          f(name, last)
        }, folderNameSuperSet, name, last)
      }
      await page.waitFor(60000);
      await clickHeaderFolder("Rodada 1", true); // abre rodada 1
      await page.waitFor(4000);
      await clickHeaderFolder("Estilos de liderança", true) // abre resultados de estilo do participante
      await page.waitFor(60000); // tempo para esperar os graficos
      await page.waitForSelector(selectors.graficoEstilos.eficaciaHeader);
      
      /**
       * Muda innerText das tags do document de Rodada 4 para Rodada 3
       */
      // await page.evaluate((legendaSelector) => {
      //   let i = 0;
      //   const legends = Array.from(document.querySelectorAll(legendaSelector))
      //   legends.forEach(l => {
      //     if (i < 2 && l.innerText.includes("Rodada 2")) {
      //       i += 1;
      //     } else if (i >= 2 && l.innerText.includes("Rodada 3")) {
      //       l.innerText = l.innerText.replace("Rodada 2", "Rodada 3");
      //       i += 1;
      //       if (i == 4) {
      //         i = 0;
      //       }
      //     } else {
      //       i = 0;
      //     }
      //   })
      // }, selectors.graficoEstilos.legenda)
      try {
        await screenshotDOMElement(openFolder, 0, -1, (await buildFileName(input.cliente, input.projeto, input.dinamica, participante.name, participante.cpf)))
      } catch (err) {
        console.error(err);
      } finally {
        await page.waitFor(4000);
        await page.keyboard.press("Escape") // fecha modal
      }
    }

    // -- * AQUI COMEÇA * -- 
    await login();
    await goToClients();
    await goToProjects();
    await goToDinamicas();
    await goToProjectAcompanhamento();
    await goToDinamicaAcompanhamento();
    const participants = (await getParticipants()).sort((a, b) => a.name < b.name);

    const jump = 0
    const total = participants.length;
    console.log(`Got ${participants.length} participants:`, participants.map(p => p.name).slice(jump, participants.length).sort());

    /**
     * 64 de 110
     * 77 -> 109 = 33
     * 0 -> 30 = 31
     * 31 -> 76 = 46
     * 
     */
    for (const participant of participants.slice(jump, total)) {
      const { cpf, name, index } = participant

      try {
        await takeSSOfParticipantEstilos(participant);
        console.log("Estilos do participante " + name + " salvos.")
      } catch (err) {
        console.error("Erro para tirar screenshot to participant with cpf " + cpf + " and name " + name);
        console.error(err)
        // throw err; // debug
        continue;
      }
    }
    browser.close()
  } catch (err) {
    throw err;
  }
}

run()
  .then(_ => {
    console.log("FIM")
    process.exit(-1)
  })
  .catch(err => {
    console.error("Erro com script.", err)
    process.exit(0);
  });
