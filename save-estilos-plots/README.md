# Configuração de ambiente para rodar o script.
- Ter o [node](https://nodejs.org/en/download/) e o [yarn](https://classic.yarnpkg.com/pt-BR/docs/install/#windows-stable) instalados. Versão 12.16.2 do node foi usado para construir o script.
- Ter ambos configurados nas variáveis de ambiente (se tiver no Windows) ou variável PATH no Linux e MacOS
- [Um bom tutorial de instalação](https://www.liquidweb.com/kb/how-to-install-yarn-on-windows/)
- Na pasta desse projeto, dê o comando (pode demorar um pouco):
```bash
  yarn install .
```
# Como executar
- Configure o objeto input que representa a entrada no arquivo index.js
```js
const input = {
  cpf: '1234',
  senha: '****',
  cliente: 'Teste Daniel',
  projeto: 'teste assíncrono',
  dinamica: 'teste grafico estilo desvincular'
}
```
- Execute pela linha de comando:
```bash
node index
```